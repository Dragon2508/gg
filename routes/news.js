const {Router} = require('express')
const News = require('../models/news')
const VideoNews = require('../models/video_news')
const router = Router()


router.get('/', async (req, res) => {
    const news = await News.find()
    const videoNews = await VideoNews.find()
    res.render('news', {
        title: 'Новости',
        active: 'news',
        news,
        videoNews
    })
})

router.post('/add_news', async (req, res) => {
    
})

router.post('/add_video', async (req, res) => {
    
})

module.exports = router