const {Router} = require('express')
const User = require('../models/user')
const authentication = require('../middleware/authentication')
const router = Router()


router.get('/', authentication, async (req, res) => {
    const coach = await User.find({ gradation: 'тренер' })
    res.render('coach', {
        title: 'Тренера',
        active: 'coach',
        coach
    })
})

module.exports = router