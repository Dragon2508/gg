const {Router} = require('express')
const User = require('../models/user')
const crypto = require('crypto')
const bcrypt = require('bcryptjs')
const nodemailer = require('nodemailer')
const sendgrid = require('nodemailer-sendgrid-transport')
const keys = require('../keys')
const resetEmail = require('../emails/reset')
const router = Router()

const transporter = nodemailer.createTransport(sendgrid({
    auth: {api_key: keys.SENDGRID_API_KEY}
}))


router.get('/', (req, res) => {
    try{
        res.render('home', {
            title: 'Глобальная игра',
            active: 'home',
            error: req.flash('error'),
            errorReset: req.flash('errorReset')
        })
    } catch (e){
        console.log(e)
    }
    
})

router.post('/login', async (req, res) => {
    try{
        const {email, password} = req.body
        const candidate = await User.findOne({email})
        if (candidate){
            const areSame = await bcrypt.compare(password, candidate.password)
            if(areSame){
                req.session.user = candidate
                req.session.isAuthentication = true
                req.session.save(err => {
                    if(err){
                        req.flash('error', 'Что-то пошло не так, повторите попытку позже!')
                        res.redirect('/')
                    } else {
                        res.redirect('/lk')
                    }
                })
            } else{
                req.flash('error', 'Пароли не совпадают!')
                res.redirect('/#login')
            }
        } else {
            req.flash('error', 'Данный пользователь не найден!')
            res.redirect('/#login')
        }
    } catch (e) {
        console.log(e)
    }
})

router.post('/reset', async (req, res) => {
    try{
        crypto.randomBytes(32, async (err, buffer) => {
            if(err){
                req.flash('errorReset', 'Что-то пошло не так, повторите попытку позже!')
                res.redirect('/')
            }

            const token = buffer.toString('hex')
            const candidate = await User.findOne({ email: req.body.email })

            if(candidate){
                candidate.resetToken = token
                candidate.resetTokenExp = Date.now() + 60 * 60 * 1000
                await candidate.save()
                await transporter.sendMail(resetEmail(candidate.email, token))
                res.redirect('/')
            } else {
                req.flash('errorReset', 'Такого email не сущесвует!')
                res.redirect('/#reset')
            }
        })
    } catch (e) {
        console.log(e)
    }
})

router.get('/logout', async (req, res) => {
    req.session.destroy(() => {
        res.redirect('/')
    })
})

module.exports = router