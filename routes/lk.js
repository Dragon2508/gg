const {Router} = require('express')
const User = require('../models/user')
const Future = require('../models/future')
const authentication = require('../middleware/authentication')
const router = Router()


router.get('/', authentication, async (req, res) => {
    res.render('lk', {
        title: 'Личный кабинет',
        active: 'lk',
        user: req.user
    })
})



router.post('/table', async (req, res) => {
    try {
        const {info} = req.body
        const candidate = Future.findById(req.params.id)
        if(candidate){
            res.redirect('/lk')
        } else {
            const future = new Future({
                info
            })
            await future.save()
            res.redirect('/lk')
        }
    } catch (e) {
        console.log(e)
    }
})

module.exports = router