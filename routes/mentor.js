const {Router} = require('express')
const User = require('../models/user')
const authentication = require('../middleware/authentication')
const router = Router()

router.get('/', authentication, async (req, res) => {
    const mentor = await User.find({ gradation: 'наставник' })
    res.render('mentor', {
        title: 'Наставники',
        active: 'mentor',
        mentor
    })
})

module.exports = router