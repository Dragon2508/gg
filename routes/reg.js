const {Router} = require('express')
const bcrypt = require('bcryptjs')
const {validationResult} = require('express-validator')
const User = require('../models/user')
const News = require('../models/news')
const nodemailer = require('nodemailer')
const sendgrid = require('nodemailer-sendgrid-transport')
const keys = require('../keys')
const regEmail = require('../emails/register')
const {registerValidators} = require('../utils/validators')
const router = Router()

const transporter = nodemailer.createTransport(sendgrid({
    auth: {api_key: keys.SENDGRID_API_KEY}
}))

router.get('/', (req, res) => {
    res.render('reg', {
        title: 'Регистрация на игру "100 дней"',
        active: 'reg',
        erorr: req.flash('registerError')
    })
})

router.post('/register', registerValidators, async (req, res) => {
    try{
        const {name, age, email, about, password, achievements, gradation, region} = req.body
        const firstText = 'В команде профессиональных тренеров появился' + name +', именно ты можешь попасть к нему в команду! Поспеши, количество мест ограничено!'
        const globalText = 'Ознакомиться с качества данного тренера можно легко, просто вступи в его команду, начни вместе с ним обучаться менеджменту, получи все необходимые знания и поведи свою команду вперед!'
        const date = new Date
        const errors = validationResult(req)
        if(!errors.isEmpty()){
            req.flash('registerError', errors.array()[0].msg)
            return res.status(422).res.redirect('/reg')
        }
        const hashPassword = await bcrypt.hash(password, 10)
        const user = new User({
            name, age, email, about, password: hashPassword, achievements, gradation, region
        })
        const news = new News({
            name, firstText, globalText, date
        })
        await user.save()
        await news.save()
        res.redirect('/')
        transporter.sendMail(regEmail(email))
    } catch (e){
        console.log(e)
    }

})

module.exports = router