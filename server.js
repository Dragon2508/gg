// LIBS
const express = require('express')
const mongoose = require('mongoose')
const session = require('express-session')
const MongoStore = require('connect-mongodb-session')(session)
const path = require('path')
const csrf = require('csurf')
const flash = require('connect-flash')
const helmet = require('helmet')
const compression = require('compression')
// LIBS
// ROUTERS
const homeRouter = require('./routes/home')
const regRouter = require('./routes/reg')
const mentorRouter = require('./routes/mentor')
const coachRouter = require('./routes/coach')
const lkRouter = require('./routes/lk')
const gameRouter = require('./routes/game')
const newsRouter = require('./routes/news')
const resetRouter = require('./routes/reset')
const agreementRouter = require('./routes/agreement')
// ROUTERS
// KEYS
const keys = require('./keys')
// KEYS
// MIDDLEWARE
const varMiddleware = require('./middleware/variables')
const userMiddleware = require('./middleware/user')
const errorHandler = require('./middleware/error404')
const fileMiddleware = require('./middleware/file')
// MIDDLEWARE


const app = express()
const store = new MongoStore({
    collection: 'session',
    uri: keys.MONGODB_URI
})

app.set('view engine', 'pug');

app.use(express.static(path.join(__dirname, 'public')))
app.use(express.urlencoded({extended: true}))

app.use(session({
    secret: keys.SESSION_SECRET,
    cookie: {
        maxAge: 1000 * 60 * 60 * 24 * 7
    },
    store: store,
    resave: false,
    saveUninitialized: false
}))
app.use(fileMiddleware.single('avatar'))
app.use(csrf())
app.use(flash())
app.use(helmet())
app.use(compression())
app.use(varMiddleware)
app.use(userMiddleware)

app.use('/', homeRouter)
app.use('/reg', regRouter)
app.use('/mentor', mentorRouter)
app.use('/lk', lkRouter)
app.use('/coach', coachRouter)
app.use('/game', gameRouter)
app.use('/news', newsRouter)
app.use('/reset', resetRouter)
app.use('/agreement', agreementRouter)

app.use(errorHandler)

const PORT = process.env.PORT || 3000

async function start() {
    try {
        await mongoose.connect(keys.MONGODB_URI, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false
        })
        app.listen(PORT, () => {
            console.log(`Server is running on port ${PORT}`)
        })
    } catch(e){
        console.log(e)
    }
}

start()