const {Schema, model} = require('mongoose')

const userSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'Future'
    },
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    age: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    about: {
        type: String,
        required: false
    },
    achievements: {
        type: String,
        required: false
    },
    gradation:{
        type: String,
        required: true
    },
    region:{
        type: String,
        required: true
    },
    team:{
        type: String,
        required: false
    },
    resetToken: String,
    resetTokenExp: Date
})

module.exports = model('User', userSchema)