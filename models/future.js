const {Schema, model} = require('mongoose')

const futureSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    info: {
        type: String,
        required: true
    }
})

module.exports = model('Future', futureSchema)