const {Schema, model} = require('mongoose')

const newsSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    name: {
        type: String,
        required: true
    },
    img: {
        type: String,
        required: false
    },
    firstText: {
        type: String,
        required: true
    },
    globalText: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        required: true
    }
})

module.exports = model('News', newsSchema)