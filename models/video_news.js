const {Schema, model} = require('mongoose')

const videoNewsSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    name: {
        type: String,
        required: true
    },
    date: {
        type: String,
        required: true
    },
    path: {
        type: String,
        required: true
    }
})

module.exports = model('VideoNews', videoNewsSchema)