$(document).ready(function () {
   $('input#terms').on('click', function () {
       if ($(this).is(':checked'))
       {
            $('.reg-btn').removeAttr('disabled');
       } else {
           $('.reg-btn').attr('disabled', 'disabled');
       } 
   });

   $('input#coach, input#mentor').on('click', function () {
        if ($(this).is(':checked')){
            $('#team').css('display', 'none');
            $('#about, #achievements').show();
        }
   });

   $('input#user').on('click', function () {
        $('#about, #achievements').css('display', 'none');
        $('#team').show();
   });

   $('.sign-up-btn').on('click', function (e) {
       e.stopPropagation();
       e.preventDefault();
      location.href = '/reg';
   });

   $('.forgot-password').on('click', function() {
        $('form.sign-in').css(
            'display', 'none'
        );
        $('form.forgot').css(
            'display', 'flex'
        );
    });

    $('.return-back').on('click', function() {
        $('form.forgot').css(
            'display', 'none'
        );
        $('form.sign-in').css(
            'display', 'flex'
        );
    });

    $('a[modal], button[modal]').on('click', function (e) {
        e.preventDefault();
        let modal = $(this).attr('data-open');
        console.log($(modal));
        $(modal).addClass('modal-open');
    });

    $('.dialogue__close').on('click', function () {
        let modal = $(this).attr('data-close');
        console.log($(modal));
        $(modal).removeClass('modal-open');
    });

    $(document).on('click', function(e) {
       if ($(e.target).hasClass('modal-open')) {
           $(e.target).removeClass('modal-open');
       }
    });

    $('.steps__study-buttons button').on('click', function () {
       let study = $(this).attr('data-study');
       $('.steps__study-buttons button').removeClass('active');
       $(this).addClass('active');
       $('.block__table').removeClass('active');
       $(study).addClass('active');
    });

    $('button.imit-video').on('click', function (e) {
        e.preventDefault();
       $('.add-video').click();
    });

    $('.add-video').on('change', function () {
        var splittedFakePath = this.value.split('\\');
        $('.selected-file').text('Файл: ' + splittedFakePath[splittedFakePath.length - 1]);
    });

    const players = Array.from(document.querySelectorAll('.video-player')).map(p => new Plyr(p));
});