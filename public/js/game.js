$('.slide_1').slick({
    prevArrow: '<img class="prev" src="img/leftChevron.svg"/>',
	nextArrow: '<img class="next" src="img/rightChevron.svg"/>',
});


var slideEl = $(".slide--parent");

slideEl.flickity({
    imagesLoaded: true,
    autoPlay: true,
    pauseAutoPlayOnHover: false,
    pageDots: true
});
